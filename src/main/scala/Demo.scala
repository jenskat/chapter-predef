import scala.concurrent.duration._

import scala.language.reflectiveCalls
import scala.language.postfixOps

import scala.Predef.require
import scala.Predef.implicitly
import scala.collection.immutable.Map
import scala.Predef.ArrowAssoc
import scala.Console.println
import scala.Predef.any2stringadd
import scala.Predef.Pair
import scala.Predef._

import scala.Array
import scala.Int
import scala.Seq
import scala.Some
import scala.List
import scala.Any
import scala.None
import scala.Unit
import scala.Long
import scala.Double
import scala.App
import scala.Tuple2

object Demo extends App {

  def addNaturals(nats: List[Int]): Int = {
    require(nats forall (_ >= 0), "List contains negative numbers")
    nats.sum
  } ensuring(_ >= 0)

  val pair = Tuple2("string", 3)

  // Will fail compilation with flag: -deprecation
  val deprecatedPair = Pair("string", 2)

  // -feature : defined in scala.language and scala.annotation.meta.languageFeature

  // postfix
  val time = 2 seconds
  val timeToString = time toString

  // language:implicitConversions
  implicit def s2i(s: String): Int = s.length

   // reflective calls
  def foo(v: {def bar()}) = v.bar()

  // -language:existentials
  type CoolExistentialSubclass = T forSome { type T <: String }

  def existentialDef(v: Seq[T] forSome { type T }): Int = 42

  // -language:higherKinds
  trait Container[M[_]] {
    def put[A](x: A): M[A]
    def get[A](m: M[A]): A
  }

  implicit val listContainer = new Container[List] { def put[A](x: A) = List(x); def get[A](m: List[A]) = m.head }
  implicit val optionContainer = new Container[Some] { def put[A](x: A) = Some(x); def get[A](m: Some[A]) = m.head }

  def tupleize[M[_]: Container, A, B](fst: M[A], snd: M[B]) = {
    val c = implicitly[Container[M]]
    c.put(c.get(fst), c.get(snd))
  }

  tupleize(Some(1), Some(2))
  tupleize(List(1), List(2))

  // -unchecked
  def unchecked[T](x: T) = x match {
    case _: Array[T] => "it's a list"
    case _ => "something else"
  }

  // return type Any will also get triggered by -Xlint
  val m: Any = Some(Map("a" -> 1, "b" -> "b"))
  val test = m match {
    case Some(m: Map[String, Any]) => println("map")
    case None => println("None")
  }

  // -Yno-adapted-args will catch this 'typo': behavior is to be remove, see -Xfuture.
  class A[T](val x: T)
  val a = new A

  val setList = List(1,2,3).toSet()
  println(setList)

  // -Ywarn-dead-code
  def foo: Int = {return 1; 1}
  def test2(nums: Array[String]): Unit =
    if (nums.length == 100) {
      if (nums.length != 100) {
        println("Dead code, so should be warned against")
      }
      println("should be printed")

    }

  // numeric-widen
  def long: Long = 100
  def doubleWidened: Double = long

  // value-discard
  def method1() = {
    println("Hello")
    "Bye"
  }
  def method2() {
    method1()
  }
  def method2_solved(): Unit = {
    method1()
    ()
  }

  // -Xfuture : which makes the compiler behave like the next major version,
  // where possible, to alert you to upcoming breaking changes.
  (1, 2) match { case Seq(_) => true; case _ => false }

  // -Ywarn-unused-imports
  import java.io.BufferedReader

  "abc".map(_.toInt)
  Array(1L,2L,3L).map(_.toString)
  val predefString = new java.util.Date
  predefString + "2"

  println(scala.Predef.any2stringadd(predefString).+("2"))

}


