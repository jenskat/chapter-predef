name := "chapter-predef"

version := "1.0"

scalaVersion := "2.11.8"

scalacOptions ++= Seq(
  "-deprecation",            // Do we use deprecated methods or types?
  "-encoding", "UTF8",      // yes, this is 2 args. let the compiler use UTF8 for the source files (also for cross-platform).
  "-feature",                 // show warning if advanced language feature is used.
//  suppress language feature warnings or import them explicitly when used import scala.language.higherKinds
  "-language:existentials",   //: https://ktoso.github.io/scala-types-of-types/#existential-types
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xlint",                  // typical linter: shadowing, return Any...
  //"-Xfuture",                 // enable flags as if we use the 'next' version of the compiler. (e.g., avoid deprecation)
  //"-Yno-adapted-args",        // Do not adapt arguments to methods or classes without argument.
  "-Ywarn-dead-code",         // give a warning when dead code is present
  "-Ywarn-numeric-widen",    // Do we widen types?
  "-Ywarn-value-discard",     // Warn when values are used
  "-Ywarn-unused-import",     // show if we don't need all imports.
  //"-Xfatal-warnings"         // fail the compilation if there are warnings
  //  HARDCORE compiler flags.
  //"-Yno-predef"              // no automatic import of Predef (removes 'useful' implicits)
  "-Yno-imports"              // no automatic imports at all; all symbols must be imported explicitly
)
